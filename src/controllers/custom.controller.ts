import { repository, Where } from "@loopback/repository";
import { get, param } from "@loopback/rest";
import { RegionRepository, DepartementRepository } from "../repositories";
import { Region, RegionRelations } from "../models";

export class CustomController {
  constructor(
    @repository(RegionRepository)
    public regionRepository: RegionRepository,
    @repository(DepartementRepository)
    public departementRepository: DepartementRepository
  ) {}

  @get("/regions/default", {
    responses: {
      "200": {
        description: "Default Regions Objects",
      },
    },
  })
  async loadDefaultRegions(
    @param.where(Region) where?: Where<Region>
  ): Promise<(Region & RegionRelations)[]> {
    return this.regionRepository.loadAll();
  }
}
