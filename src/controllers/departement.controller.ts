import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from "@loopback/repository";
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from "@loopback/rest";
import { Departement } from "../models";
import { DepartementRepository, RegionRepository } from "../repositories";
import { departs2Regions } from "../assets/departs2Regions";

export class DepartementController {
  constructor(
    @repository(DepartementRepository)
    public departementRepository: DepartementRepository,
    @repository(RegionRepository)
    public regionRepository: RegionRepository
  ) {}

  @post("/departements/default", {
    responses: {
      "200": {
        description: "RegDepartement ion model instance",
        content: {
          "application/json": { schema: getModelSchemaRef(Departement) },
        },
      },
    },
  })
  async default(
    @requestBody({
      content: {
        "application/json": {
          schema: getModelSchemaRef(Departement, {
            title: "NewRegion",
            exclude: ["departId"],
          }),
        },
      },
    })
    region: Omit<Departement, "departId">
  ): Promise<Departement[] | void> {
    this.departementRepository.loadAll();
  }

  @post("/departements", {
    responses: {
      "200": {
        description: "Departement model instance",
        content: {
          "application/json": { schema: getModelSchemaRef(Departement) },
        },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        "application/json": {
          schema: getModelSchemaRef(Departement, {
            title: "NewDepartement",
          }),
        },
      },
    })
    departement: Departement
  ): Promise<Departement | Partial<Departement>> {
    return await new departs2Regions(
      this.regionRepository,
      this.departementRepository
    ).depart2Region(departement);
  }

  @get("/departements/count", {
    responses: {
      "200": {
        description: "Departement model count",
        content: { "application/json": { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.where(Departement) where?: Where<Departement>
  ): Promise<Count> {
    return this.departementRepository.count(where);
  }

  @get("/departements", {
    responses: {
      "200": {
        description: "Array of Departement model instances",
        content: {
          "application/json": {
            schema: {
              type: "array",
              items: getModelSchemaRef(Departement, {
                includeRelations: true,
              }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(Departement) filter?: Filter<Departement>
  ): Promise<Departement[]> {
    return this.departementRepository.find(filter);
  }

  @patch("/departements", {
    responses: {
      "200": {
        description: "Departement PATCH success count",
        content: { "application/json": { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        "application/json": {
          schema: getModelSchemaRef(Departement, { partial: true }),
        },
      },
    })
    departement: Departement,
    @param.where(Departement) where?: Where<Departement>
  ): Promise<Count> {
    return this.departementRepository.updateAll(departement, where);
  }

  @get("/departements/{id}", {
    responses: {
      "200": {
        description: "Departement model instance",
        content: {
          "application/json": {
            schema: getModelSchemaRef(Departement, {
              includeRelations: true,
            }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string("id") id: string,
    @param.filter(Departement, { exclude: "where" })
    filter?: FilterExcludingWhere<Departement>
  ): Promise<Departement> {
    return this.departementRepository.findById(id, filter);
  }

  @patch("/departements/{id}", {
    responses: {
      "204": {
        description: "Departement PATCH success",
      },
    },
  })
  async updateById(
    @param.path.string("id") id: string,
    @requestBody({
      content: {
        "application/json": {
          schema: getModelSchemaRef(Departement, { partial: true }),
        },
      },
    })
    departement: Departement
  ): Promise<void> {
    await this.departementRepository.updateById(id, departement);
  }

  @put("/departements/{id}", {
    responses: {
      "204": {
        description: "Departement PUT success",
      },
    },
  })
  async replaceById(
    @param.path.string("id") id: string,
    @requestBody() departement: Departement
  ): Promise<void> {
    await this.departementRepository.replaceById(id, departement);
  }

  @del("/departements/{id}", {
    responses: {
      "204": {
        description: "Departement DELETE success",
      },
    },
  })
  async deleteById(@param.path.string("id") id: string): Promise<void> {
    // await this.departementRepository.deleteById(id);
    await new departs2Regions(
      this.regionRepository,
      this.departementRepository
    ).deleteDepartement(id);
  }
}
