import { BootMixin } from "@loopback/boot";
import { ApplicationConfig } from "@loopback/core";
import {
  RestExplorerBindings,
  RestExplorerComponent,
} from "@loopback/rest-explorer";
import { RepositoryMixin, SchemaMigrationOptions } from "@loopback/repository";
import { RestApplication } from "@loopback/rest";
import { ServiceMixin } from "@loopback/service-proxy";
import path from "path";
import { MySequence } from "./sequence";
import { DepartementRepository } from "./repositories/departement.repository";
import { RegionRepository } from "./repositories/region.repository";
import { departs2Regions } from "./assets/departs2Regions";

export { ApplicationConfig };
export class BacneApplication extends BootMixin(
  ServiceMixin(RepositoryMixin(RestApplication))
) {
  constructor(options: ApplicationConfig = {}) {
    super(options);

    // Set up the custom sequence
    this.sequence(MySequence);

    // Set up default home page
    this.static("/", path.join(__dirname, "../public"));

    // Customize @loopback/rest-explorer configuration here
    this.configure(RestExplorerBindings.COMPONENT).to({
      path: "/explorer",
    });
    this.component(RestExplorerComponent);

    this.projectRoot = __dirname;
    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        // Customize ControllerBooter Conventions here
        dirs: ["controllers"],
        extensions: [".controller.js"],
        nested: true,
      },
    };
  }

  async migrateSchema(options?: SchemaMigrationOptions) {
    /**
     * Run migration scripts provided by connectors
     */
    await super.migrateSchema(options);
  }

  async migrateDefaultRegions() {
    /**
     * Insert Regions in database by default
     * List of the regions to be found in assets/regions.default.ts
     */
    const regionRepo = this.getRepository(RegionRepository);
    return (await regionRepo).loadAll();
  }

  async migrateDefaultDepartements() {
    /**
     * Insert Departement in database by default
     * List of the departements to be found in assets/departements.default.ts
     */
    const departRepo = this.getRepository(DepartementRepository);
    return (await departRepo).loadAll();
  }

  async departs2Regions() {
    /**
     * Should add departements to the Regions if these ones are not existing yet
     */
    const regionRepo = await this.getRepository(RegionRepository);
    const departRepo = await this.getRepository(DepartementRepository);
    return new departs2Regions(regionRepo, departRepo).sync();
  }
}
