import { ApplicationConfig, BacneApplication } from "./application";
import { loadDefaultRegions } from "./requests/request";

export * from "./application";
export async function main(options: ApplicationConfig = {}) {
  const app = new BacneApplication(options);
  await app.boot();
  await app.start();
  loadDefaultRegions();

  /**
   * The bloc below is added in order to load some default data
   * Default Regions are located in assets/regions.default.ts
   * Default Departements are located in assets/departements.default.ts
   */
  /*
  await app.migrateSchema();
  await app.migrateDefaultRegions();
  await app.migrateDefaultDepartements();
  await app.departs2Regions();
  */
  /**
   *
   */
  const url = app.restServer.url;
  console.log(`Server is running at ${url}`);
  console.log(`Try ${url}/ping`);
  return app;
}

if (require.main === module) {
  // Run the application
  const config = {
    rest: {
      port: +(process.env.PORT ?? 3000),
      host: process.env.HOST,
      // The `gracePeriodForClose` provides a graceful close for http/https
      // servers with keep-alive clients. The default value is `Infinity`
      // (don't force-close). If you want to immediately destroy all sockets
      // upon stop, set its value to `0`.
      // See https://www.npmjs.com/package/stoppable
      gracePeriodForClose: 5000, // 5 seconds
      openApiSpec: {
        // useful when used with OpenAPI-to-GraphQL to locate your application
        setServersFromRequest: true,
      },
    },
  };
  main(config).catch((err) => {
    console.error("Cannot start the application.", err);
    process.exit(1);
  });
}
