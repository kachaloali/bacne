import { Region } from "../models/region.model";

export const regionsDefault: Partial<Region>[] = [
  {
    name: "Agadez",
    regionCode: "NE-1",
    geolocalisation: "16°58′0″N 7°59′0″E",
    population: 566447,
    nbCommunes: 15,
  },
  {
    name: "Diffa",
    regionCode: "NE-2",
    geolocalisation: "13°18′53″N 12°37′4″E",
    population: 669307,
    nbCommunes: 12,
  },
  {
    name: "Dosso",
    regionCode: "NE-3",
    geolocalisation: "13°03′N 3°12′E",
    population: 2368651,
    nbCommunes: 43,
  },
  {
    name: "Maradi",
    regionCode: "NE-4",
    geolocalisation: "13°30′N 7°6′E",
    population: 4160231,
    nbCommunes: 47,
  },
  {
    name: "Tahoua",
    regionCode: "NE-5",
    geolocalisation: "14°53′N 5°16′E",
    population: 1475826,
    nbCommunes: 42,
  },
  {
    name: "Tillabéri",
    regionCode: "NE-6",
    geolocalisation: "14°13′0′N,1°27′0′E",
    population: 3155731,
    nbCommunes: 45,
  },
  {
    name: "Zinder",
    regionCode: "NE-7",
    geolocalisation: "13°48′19″N 8°59′18″E",
    population: 4132321,
    nbCommunes: 55,
  },
  {
    name: "Niamey",
    regionCode: "NE-8",
    geolocalisation: "13°30′42″N 2°7′31″E",
    population: 1164826,
    nbCommunes: 0,
  },
];
