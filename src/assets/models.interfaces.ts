export interface IModelCollection<TypeObject> {
  add(key: string, value: TypeObject): void;
  containsKey(key: string): boolean;
  count(): number;
  item(key: string): TypeObject;
  keys(): string[];
  remove(key: string): TypeObject;
  values(): TypeObject[];
}
