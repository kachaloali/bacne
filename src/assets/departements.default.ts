import { Departement } from "../models/departement.model";

export const departementsDefault: Partial<Departement>[] = [
  {
    name: "Tchirozerine",
    rcode: "NE-1",
  },
  {
    name: "Ingal",
    rcode: "NE-1",
  },
  {
    name: "Iferouane",
    rcode: "NE-1",
  },
  {
    name: "Bilma",
    rcode: "NE-1",
  },
  {
    name: "Arlit",
    rcode: "NE-1",
  },
  {
    name: "Aderbissinat",
    rcode: "NE-1",
  },
  {
    name: "Bosso",
    rcode: "NE-2",
  },

  {
    name: "Goudoumaria",
    rcode: "NE-2",
  },
  {
    name: "Mainé-Soroa",
    rcode: "NE-2",
  },

  {
    name: "N’Guigmi",
    rcode: "NE-2",
  },
  {
    name: "N’Gourti",
    rcode: "NE-2",
  },
  {
    name: "Boboye",
    rcode: "NE-3",
  },
  {
    name: "Dioundiou",
    rcode: "NE-3",
  },
  {
    name: "Dogondoutchi",
    rcode: "NE-3",
  },
  {
    name: "Falmey",
    rcode: "NE-3",
  },
  {
    name: "Gaya",
    rcode: "NE-3",
  },
  {
    name: "Loga",
    rcode: "NE-3",
  },
  {
    name: "Tibiri",
    rcode: "NE-3",
  },
  {
    name: "Aguié",
    rcode: "NE-4",
  },
  {
    name: "Bermo",
    rcode: "NE-4",
  },
  {
    name: "Dakoro",
    rcode: "NE-4",
  },
  {
    name: "Gazaoua",
    rcode: "NE-4",
  },
  {
    name: "Guidan-Roumdji",
    rcode: "NE-4",
  },
  {
    name: "Madarounfa",
    rcode: "NE-4",
  },
  {
    name: "Mayayi",
    rcode: "NE-4",
  },
  {
    name: "Tessaoua",
    rcode: "NE-4",
  },
  {
    name: "Abalak",
    rcode: "NE-5",
  },

  {
    name: "Bagaroua",
    rcode: "NE-5",
  },
  {
    name: "Birni N'Konni",
    rcode: "NE-5",
  },

  {
    name: "Bouza",
    rcode: "NE-5",
  },
  {
    name: "Illéla",
    rcode: "NE-5",
  },
  {
    name: "Kéita",
    rcode: "NE-5",
  },
  {
    name: "Madaoua",
    rcode: "NE-5",
  },
  {
    name: "Malbaza",
    rcode: "NE-5",
  },
  {
    name: "Tassara",
    rcode: "NE-5",
  },
  {
    name: "Tchitabaraden",
    rcode: "NE-5",
  },
  {
    name: "Tillia",
    rcode: "NE-5",
  },

  {
    name: "Torodi",
    rcode: "NE-6",
  },

  {
    name: "Téra",
    rcode: "NE-6",
  },
  {
    name: "Say",
    rcode: "NE-6",
  },

  {
    name: "Ouallam",
    rcode: "NE-6",
  },
  {
    name: "Kollo",
    rcode: "NE-6",
  },
  {
    name: "Abala",
    rcode: "NE-6",
  },
  {
    name: "Ayérou",
    rcode: "NE-6",
  },
  {
    name: "Balleyara",
    rcode: "NE-6",
  },
  {
    name: "Banibangou",
    rcode: "NE-6",
  },
  {
    name: "Bankilaré",
    rcode: "NE-6",
  },
  {
    name: "Fillingé",
    rcode: "NE-6",
  },
  {
    name: "Gotheye",
    rcode: "NE-6",
  },
  {
    name: "Belbédji",
    rcode: "NE-7",
  },
  {
    name: "Damagaram Takaya",
    rcode: "NE-7",
  },
  {
    name: "Dungass",
    rcode: "NE-7",
  },
  {
    name: "Gouré",
    rcode: "NE-7",
  },
  {
    name: "Kantché",
    rcode: "NE-7",
  },
  {
    name: "Magaria",
    rcode: "NE-7",
  },

  {
    name: "Mirriah",
    rcode: "NE-7",
  },
  {
    name: "Takeita",
    rcode: "NE-7",
  },
  {
    name: "Tanout",
    rcode: "NE-7",
  },
  {
    name: "Tesker",
    rcode: "NE-7",
  },
];
