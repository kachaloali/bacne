import { IModelCollection } from "./models.interfaces";

// Should be a collection of all types of Models/Objects
export class ModelCollection<TypeObject>
  implements IModelCollection<TypeObject> {
  //private counter: number = 0;
  private items: { [index: string]: TypeObject } = {};

  public containsKey(key: string): boolean {
    return this.items.hasOwnProperty(key);
  }

  public count(): number {
    return this.keys().length;
  }

  public add(key: string, value: TypeObject) {
    if (!this.items.hasOwnProperty(key)) 
    //this.counter++;
    this.items[key] = value;
  }

  public remove(key: string): TypeObject {
    var val = this.items[key];
    delete this.items[key];
    //this.counter--;
    return val;
  }

  public item(key: string): TypeObject {
    return this.items[key];
  }

  public keys(): string[] {
    var keySet: string[] = [];
    for (var prop in this.items) {
      if (this.items.hasOwnProperty(prop)) {
        keySet.push(prop);
      }
    }
    return keySet;
  }

  public values(): TypeObject[] {
    var values: TypeObject[] = [];
    for (var prop in this.items) {
      if (this.items.hasOwnProperty(prop)) {
        values.push(this.items[prop]);
      }
    }
    return values;
  }
}
