import { ApplicationConfig } from "@loopback/core";
import { RegionRepository, DepartementRepository } from "../repositories";
import { repository } from "@loopback/repository";
import { Departement } from "../models";

export { ApplicationConfig };
export class departs2Regions {
  constructor(
    @repository(RegionRepository) private _regionRepo: RegionRepository,
    @repository(DepartementRepository)
    private _departRepo: DepartementRepository
  ) {}

  async sync() {
    /**
     * This function is mapping and synchronize Departements to Regions
     * Each Departement belongs to a Region.
     *
     * The operation here is to associate all Departements to the Regions
     * In which they are belonging to.
     */
    let _regions = this._regionRepo;
    let _departs = this._departRepo;
    //
    let result = (await _regions.find()).map(async (region) => {
      region.departements = (await _departs.find()).filter((depart) => {
        region.regionCode === depart.rcode
          ? (depart.regionId = region.regionId)
          : "NER";
        return region.regionCode === depart.rcode;
      });
      // console.log(region.name, region.departements.length);
      return region;
    });
    //
    for (const region of result) {
      await _regions.updateById((await region).regionId, await region);
    }
    return _regions.find();
  }

  async depart2Region(_depart: Departement | Partial<Departement>) {
    /**
     * This create a Departement and then link it to the Region in which
     * it is belonging to. If the Region does not exist, the Departement
     * Is not created.
     */
    let filter = {
      where: {
        regionCode: _depart.rcode,
      },
      fields: {},
      limit: 1,
    };
    //
    let departTobe = _depart;
    await this._regionRepo.findOne(filter).then(async (region) => {
      if (region) {
        _depart.regionId = region.regionId;
        await this._departRepo.create(_depart).then((depart) => {
          departTobe = depart;
          region.departements.push(depart);
          this._regionRepo.updateById(region.regionId, region);
        });
      } else {
        console.log(
          "Region ayant pour code [" +
            _depart.rcode +
            "] n'a pas pu être trouvée"
        );
      }
    });
    return departTobe;
  }

  async deleteDepartement(_departId: string) {
    /**
     * Should find the Departement and delete it from the database
     * The Departement should also be removed from the Region it is associating
     */
    let depart = await this._departRepo.findById(_departId);
    if (depart) {
      let filter = {
        where: {
          regionCode: depart.rcode,
        },
        fields: {},
        limit: 1,
      };
      let region = await this._regionRepo.findOne(filter);
      if (region) {
        let index = -1;
        region.departements.find(function (elt, i) {
          index = i;
          return elt.departId === depart.departId;
        });
        if (index >= 0) {
          region.departements.splice(index, 1);
          await this._regionRepo.updateById(region.regionId, region);
        }
        this._departRepo.deleteById(_departId);
      }
    }
  }
}
