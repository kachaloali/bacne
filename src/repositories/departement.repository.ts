import { DefaultCrudRepository, repository } from "@loopback/repository";
import { Departement, DepartementRelations, Region } from "../models";
import { BacneDataSource } from "../datasources";
import { inject } from "@loopback/core";
import { departementsDefault } from "../assets/departements.default";

export class DepartementRepository extends DefaultCrudRepository<
  Departement,
  typeof Departement.prototype.departId,
  DepartementRelations
> {
  constructor(@inject("datasources.bacne") dataSource: BacneDataSource) {
    super(Departement, dataSource);
  }

  async loadAll() {
    /**
     * Iterate all reigions of assets/regions.default and insert them
     * into the database. This operation is done for each app launch
     * This is a temporary implementation for dev purpose
     */
    departementsDefault.forEach(async (depart) => {
      await this.findOrCreate(depart)
        .then((doc) => {
          if (!doc) {
            console.log("Depart: [" + depart.name + "] n'a pas pu être créé");
          }
        })
        .catch((err) => {
          throw err;
        });
    });
    console.log("Default Departements created");
    return this.find();
  }

  async findOrCreate(depart: Departement | Partial<Departement>) {
    /**
     * This function test if a region exist. If not the region is created
     */
    let filter = {
      where: {
        departId: depart.departId,
      },
      fields: {},
      limit: 1,
    };
    return this.find(filter)
      .then((doc) => {
        if (doc.length === 0) {
          return this.createAll([depart]);
        } else {
          return this.find(filter);
        }
      })
      .catch((err) => console.log(err));
  }
}
