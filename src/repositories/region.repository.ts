import { DefaultCrudRepository } from "@loopback/repository";
import { Region, RegionRelations, Departement } from "../models";
import { BacneDataSource } from "../datasources";
import { inject } from "@loopback/core";
import { regionsDefault } from "../assets/regions.default";

export class RegionRepository extends DefaultCrudRepository<
  Region,
  typeof Region.prototype.regionId,
  RegionRelations
> {
  constructor(@inject("datasources.bacne") dataSource: BacneDataSource) {
    super(Region, dataSource);
  }

  async loadAll() {
    /**
     * Iterate all Regions of assets/regions.default and insert them
     * into the database. This operation is done for each app launch
     */
    regionsDefault.forEach(async (region) => {
      await this.findOrCreate(region)
        .then((doc) => {
          if (!doc) {
            console.log("Region: [" + region.name + "] n'a pas pu etre créée");
          }
        })
        .catch((err) => {
          throw err;
        });
    });
    console.log("Default Regions created");
    return this.find();
  }

  async findByCode(_code: string) {
    /**
     * The function find an existing region in the database knowing the code
     * This function is created for DEV purpose. It will be removed further
     */
    let filter = {
      limit: 1,
      where: {
        regionCode: _code,
      },
      fields: {},
    };
    return this.findOne(filter);
  }

  async findOrCreate(region: Region | Partial<Region>) {
    /**
     * This function test if a region exist. If not the region is created
     */
    let filter = {
      where: {
        regionCode: region.regionCode,
      },
      fields: {},
      limit: 1,
    };
    return this.find(filter)
      .then((doc) => {
        if (doc.length === 0) {
          return this.createAll([region]);
        } else {
          return this.find(filter);
        }
      })
      .catch((err) => console.log(err));
  }
}
