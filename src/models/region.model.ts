import { Entity, model, property } from "@loopback/repository";
import { Departement } from "./departement.model";

@model()
export class Region extends Entity {
  @property({
    type: "string",
    id: true,
    generated: true,
  })
  regionId?: string;

  @property({
    type: "string",
    required: true,
  })
  name: string;

  @property({
    type: "string",
    required: true,
  })
  regionCode: string;

  @property({
    type: "string",
    default: "Niger",
  })
  geolocalisation?: string;

  @property({
    type: "number",
    default: 0,
  })
  population?: number;

  @property({
    type: "number",
    default: 0,
  })
  nbCommunes?: number;

  @property.array(Departement, {
    default: [],
  })
  departements: Partial<Departement>[];

  constructor(data?: Partial<Region>) {
    super(data);
  }
}

export interface RegionRelations {
  // describe navigational properties here
}

export type RegionWithRelations = Region & RegionRelations;
