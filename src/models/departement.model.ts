import {Entity, model, property} from '@loopback/repository';

@model()
export class Departement extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  departId?: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    default: "NER",
  })
  geolocalisation?: string;

  @property({
    type: 'number',
    default: 0,
  })
  population?: number;

  @property({
    type: 'number',
    default: 0,
  })
  nbCommunes?: number;

  @property({
    type: 'string',
    required: true,
  })
  rcode: string;

  @property({
    type: 'string',
    default: "NER",
  })
  regionId?: string;


  constructor(data?: Partial<Departement>) {
    super(data);
  }
}

export interface DepartementRelations {
  // describe navigational properties here
}

export type DepartementWithRelations = Departement & DepartementRelations;
