import http from "http";
export const loadDefaultRegions = function () {
  const regionOptions = {
    method: "GET",
    host: "localhost",
    port: 3000,
    path: "/regions/default",
  };
  const regionRequest = http.request(regionOptions);
  regionRequest.end();
};
module.exports = { loadDefaultRegions };
